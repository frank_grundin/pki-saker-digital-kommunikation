# Generate truststore for public usage

mkdir -p generated
rm generated/*.*

echo "********************************************************************"
echo "*   Truststore for qa env                                         *"
echo "********************************************************************"

echo "GENERATE QA TRUSTSTORE ==>"
# Telsec cert is used for SMP (validation of Signed Service Metadata)
keytool -importcert -alias telsec_root_ca -storepass password -noprompt -trustcacerts -file qa-env/TeleSec_Business_CA_T-TeleSec_GlobalRoot_Class.cer -keystore generated/qa_truststore.jks
# SDK Testklient
keytool -importcert -alias ipaas_rapidssl_ca -storepass password -noprompt -trustcacerts -file qa-env/cert_RapidSSLRSACA2018 -keystore generated/qa_truststore.jks
keytool -import -v -trustcacerts -alias sdk-test.nordicmedtest.se -storepass password -noprompt -file qa-env/sdk-test.pem -keystore generated/qa_truststore.jks

# Identrust 
keytool -importcert -alias letsencrypt_root_ca -storepass password -noprompt -trustcacerts -file qa-env/letsencrypt_root_ca.pem -keystore generated/qa_truststore.jks
keytool -import -v -trustcacerts -alias sundbyberg_identrust -storepass password -noprompt -file qa-env/sundbyberg.pem -keystore generated/qa_truststore.jks
# GlobalSign
keytool -importcert -alias globalsign_root_ca -storepass password -noprompt -trustcacerts -file qa-env/gsorganizationvalsha2g2r1.crt -keystore generated/qa_truststore.jks
keytool -import -v -trustcacerts -alias af_globalsign -storepass password -noprompt -file qa-env/af.pem -keystore generated/qa_truststore.jks
keytool -import -v -trustcacerts -alias cert_test-sdk.sundbyberg.se -storepass password -noprompt -file qa-env/cert_test-sdk.sundbyberg.se -keystore generated/qa_truststore.jks
# SITHS eID
keytool -importcert -alias siths_root_ca -storepass password -noprompt -trustcacerts -file qa-env/testsithseidfunctioncav1.cer -keystore generated/qa_truststore.jks
keytool -import -v -trustcacerts -alias ostersund_siths -storepass password -noprompt -file qa-env/sdk-test_ostersund_se-pub.cer -keystore generated/qa_truststore.jks
keytool -import -v -trustcacerts -alias regionVarmland_siths -storepass password -noprompt -file qa-env/cert_sdk-pilot.regionvarmland.se -keystore generated/qa_truststore.jks
# Telia
keytool -importcert -alias telia_root_ca -storepass password -noprompt -trustcacerts -file qa-env/cert_TeliaSoneraServerCAv2 -keystore generated/qa_truststore.jks
keytool -import -v -trustcacerts -alias varmdo_telia -storepass password -noprompt -file qa-env/cert_sdk-test.varmdo.se -keystore generated/qa_truststore.jks
# Thawte
keytool -importcert -alias thawte_root_ca -storepass password -noprompt -trustcacerts -file qa-env/certThawteTLSRSACAG1 -keystore generated/qa_truststore.jks
keytool -import -v -trustcacerts -alias nacka_thawte -storepass password -noprompt -file qa-env/cert_sdk-test.nacka.se -keystore generated/qa_truststore.jks
# Sectigo
keytool -import -v -trustcacerts -alias norrtalje_sectigo -storepass password -noprompt -file qa-env/cert_test-sdk.norrtalje.se -keystore generated/qa_truststore.jks
#Digicert
keytool -import -v -trustcacerts -alias skl_digicert -storepass password -noprompt -file qa-env/cert_test-sdk.skl.se -keystore generated/qa_truststore.jks
# Self-signed
keytool -import -v -trustcacerts -alias skovde_selfsign -storepass password -noprompt -file qa-env/skovde.pem -keystore generated/qa_truststore.jks
echo "<==GENERATE QA TRUSTSTORE"


echo "********************************************************************"
echo "*   Finished generating stores to be used for QA env               *"
echo "*                                                                  *"
echo "*   See /generated folder for the resulting *.jks files to be used *"
echo "********************************************************************"
