
# PKI - Säker Digital Kommunikation

## Addera ny organisation
Uppdatera *./generate_truststore.sh* med ytterligare en rad enligt nedan:

*keytool -import -v -trustcacerts -alias ORGANISATIONSNAMN_CERTTYP -storepass password -noprompt -file qa-env/PUBLICCERT -keystore generated/qa_truststore.jks*

- ORGANISATIONSNAMN - Hämtas från anslutningsblankett 
- PUBLICCERT - Publika certifikatet hämtas från anslutningsblankett
- CERTTYP - Kort beskrivning vilken typ av cert som används, hämtas från anslutningsblankett, dubbelkolla även på följande sätt:
*openssl x509 -in PUBLICCERT -inform pem -text* 

## Uppdatera truststore
*./generate_truststore.sh*

Uppdatera Domibus truststore i SDK Testbädd:
[https://at-sdk.ipaas.inera.se/domibus/login?returnUrl=%2F]

Kopiera *./generated/qa_truststore.jks* till:
[https://inera.atlassian.net/wiki/spaces/OISDK/pages/4032583/Anslutningsinformation+teknik]

## Publicera giltighetstider på de publika certifikaten
*keytool -list -v -keystore generated/qa_truststore.jks | grep -B7 "Valid from:" | grep -v "Creation date:" | grep -v "Entry type:" | grep -v "Serial number:" | grep -v -e '^$' > ./generated/certsValidityInfo*

Kopiera innehållet från *./generated/certsValidityInfo* till:
[https://inera.atlassian.net/wiki/spaces/OISDK/pages/4032583/Anslutningsinformation+teknik] 
